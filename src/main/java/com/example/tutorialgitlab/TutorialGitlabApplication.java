package com.example.tutorialgitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialGitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutorialGitlabApplication.class, args);
	}

}
